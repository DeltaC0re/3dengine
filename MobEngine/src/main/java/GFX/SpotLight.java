package GFX;

import org.lwjgl.util.vector.Vector3f;

public class SpotLight extends Light{

	private float range = 1.0f;
	
	public SpotLight(int intensity, Vector3f pos, float circleRange) {
		super(intensity, pos);
		this.setRange(circleRange);
	}
	
	public float getRange() {
		return range;
	}

	public void setRange(float range) {
		this.range = range;
	}

}
