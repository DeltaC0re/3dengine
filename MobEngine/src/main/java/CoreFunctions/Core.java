package CoreFunctions;

import org.lwjgl.opengl.Display;

public class Core {

	public Core(MobEngine engine) {
		engine.init();
		while (!Display.isCloseRequested()) {
			Display.update();
			engine.renderHud();
			engine.update();
			engine.render();
			//engine.renderHud();
			if(!engine.isVsync()){
				Display.sync(engine.getFps());
			}
		}
		Display.destroy();
		System.exit(0);
	}

}
